Source: fxt
Priority: optional
Maintainer: Samuel Thibault <sthibault@debian.org>
Uploaders: Vincent Danjean <vdanjean@debian.org>
Build-Depends: debhelper-compat (= 12), gawk, help2man
Build-Conflicts: binutils-dev
Rules-Requires-Root: no
Standards-Version: 4.5.0
Section: libs
Homepage: https://savannah.nongnu.org/p/fkt
Vcs-Git: https://salsa.debian.org/debian/fxt.git
Vcs-Browser: https://salsa.debian.org/debian/fxt

Package: libfxt-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}, libfxt2 (= ${binary:Version})
Description: Multithreaded tracing library
 FxT is a library and associated tools that can be used to analyze the
 performance of multithreaded programs which can potentially use a
 hybrid thread scheduler (i.e. a user-level scheduler on top of a
 kernel-level one). The Marcel thread library can take full profit from
 this library.
 .
 FxT is based on the offline analysis of traces (sequence of events recorded at
 run time).
 .
 This package contains static libraries and development headers.

Package: libfxt2
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Multithreaded tracing library
 FxT is a library and associated tools that can be used to analyze the
 performance of multithreaded programs which can potentially use a
 hybrid thread scheduler (i.e. a user-level scheduler on top of a
 kernel-level one). The Marcel thread library can take full profit from
 this library.
 .
 FxT is based on the offline analysis of traces (sequence of events recorded at
 run time).
 .
 This package contains shared libraries.

Package: fxt-tools
Section: science
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Multithreaded tracing library
 FxT is a library and associated tools that can be used to analyze the
 performance of multithreaded programs which can potentially use a
 hybrid thread scheduler (i.e. a user-level scheduler on top of a
 kernel-level one). The Marcel thread library can take full profit from
 this library.
 .
 FxT is based on the offline analysis of traces (sequence of events recorded at
 run time).
 .
 This package contains tools.
